import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data set for login', () => {
    let invalidCredentialsDataSet = [
        { email: 'usr2@gmail.com', password: '' },
        { email: 'usr2@gmail.com', password: '      ' },
        { email: 'usr2@gmail.com', password: 'password! ' },
        { email: 'usr2@gmail.com', password: 'passwrd2' },
        { email: 'usr2@gmail.com', password: 'admin' },
        { email: 'usr2@gmail.com', password: 'usr2@gmail.com' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });
});


