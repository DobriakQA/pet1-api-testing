import { expect } from "chai";
import { List_usersController } from "../lib/controllers/list_users.controller";
import { equal } from "assert";
const list_users = new List_usersController();

describe("List_users controller", () => {
    it('getAllUsers', async () => {
        let response = await list_users.getAllUsers();

        // console.log(response.body);

        expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time should be less than 7s').to.be.lessThan(5000);
        expect(response.body.length).to.be.greaterThan(1)
    });
});






