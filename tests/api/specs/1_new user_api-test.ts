import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";

const users = new UsersController();
const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
    let userId: number;

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 2434,
            avatar: "string",
            email: "usr2@gmail.com",
            userName: "User2",
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(201);
    });
});